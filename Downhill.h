﻿#ifndef DOWNHILL_H
#define DOWNHILL_H

#include <SkiModule.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/LeftLeg.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/RightLeg.h>
#include <darwin/framework/DoublePidController.h>

/*****************************************************************************
 *
 * Dynamic downhill skiing module
 *
 * Uses PID controllers to adjust the ankle pitch and leg lengths
 * to keep both skis on the hill when going up/down hill and across the slope
 *
 * TODO: carving turns
 *
 *****************************************************************************/

class Downhill : public SkiModule
{
public:
    static Downhill *GetInstance(){return uniqueInstance;}

    virtual ~Downhill();

    virtual void Initialize();
    virtual void Process();

    static const int NO_CARVE = -1;

    virtual void Stop();
    virtual void Start(int numTurns = NO_CARVE);

    virtual void SetEnable(bool enable, bool exclusive = false);

    virtual void LoadIniSettings(minIni *ini,std::string section="Downhill Config");

    // turn left (negative) or right (positive)
    // this stops the carving and holds the turn
    virtual void Turn(double angle);

    // go into a snowplow position
    // this will stop any carving and hold the brake
    static const double FULL_BRAKE_AMOUNT = 30.0;
    static const double LIGHT_BRAKE_AMOUNT = 10.0;
    static const double NO_BRAKE_AMOUNT = 0.0;
    virtual void Brake(double amount=FULL_BRAKE_AMOUNT);

private:
    static Downhill *uniqueInstance;
    Downhill();

    double BRAKE_INTENSITY; // <=0: no braking, >0: intensity determines snowplow angle & ankle roll for edge-dig-in
    double TURN_INTENSITY;  // ==0: no turn, >0: right turn, <0: left turn

    // downhill carving parameters
    double turnSlideGain;
    double brakeYawGain;
    int carvePeriod; // # of ticks for one complete left-right carve cycle
    double holdTurnRatio;
    double holdStraightRatio;
    double carveAmplitude;
    int turnsRemaining;
    int tickCounter;

    void enterStartingPosition();
    void correctForSlope();     // adjust the pitch and roll so that the skis are flush with the slope; slews the torso left/right for turning
    void applyYawCorrection();

    void doCarve();
    void applyTurn();
    void applyBrake();

    enum CARVE_STATE{
        CARVE_STRAIGHT1 = 0,
        CARVE_TURN_R,
        CARVE_HOLD_R,
        CARVE_UNTURN_R,
        CARVE_STRAIGHT2,
        CARVE_TURN_L,
        CARVE_HOLD_L,
        CARVE_UNTURN_L,

        NUM_CARVE_STATES
    };
    int carveState;

    bool isRollerSkis;
};

#endif // DOWNHILL_H
