#include "Downhill.h"
#include <iostream>
#include <cmath>
#include <darwin/framework/Math.h>
#include <darwin/framework/Kinematics.h>
#include <algorithm>

using namespace Robot;
using namespace std;

Downhill *Downhill::uniqueInstance = new Downhill();

Downhill::Downhill() : SkiModule()
{
    BRAKE_INTENSITY = 0;
    TURN_INTENSITY = 0;
}

Downhill::~Downhill()
{
}

void Downhill::Initialize()
{
    SkiModule::Initialize();
}

void Downhill::LoadIniSettings(minIni *ini, string section)
{
    SkiModule::LoadIniSettings(ini,section);

    turnSlideGain = ini->getd(section,"turn_slide_gain");
    brakeYawGain = ini->getd(section,"brake_yaw_gain",1.0);
    carvePeriod = ini->geti(section,"carve_period",0) / TIME_UNIT;
    carveAmplitude = ini->getd(section,"carve_amplitude");
    holdTurnRatio = ini->getd(section,"hold_turn_ratio");
    holdStraightRatio = ini->getd(section,"hold_straight_ratio");

    isRollerSkis = ini->geti(section,"roller_skis",0) != 0;
}

void Downhill::enterStartingPosition()
{
    Point3D leftLegPosition(xOffset,yOffset,-hipHeight);
    Point3D rightLegPosition(xOffset,yOffset,-hipHeight);
    leftLeg.SetGoalPosition(leftLegPosition,hipYawOffset);
    rightLeg.SetGoalPosition(rightLegPosition,hipYawOffset);
    leftLeg.Process();
    rightLeg.Process();

    // set the arm positions
    leftArm.SetAngles(shoulderPitchOffset,shoulderRollOffset,elbowOffset);
    rightArm.SetAngles(shoulderPitchOffset,shoulderRollOffset,elbowOffset);
    leftArm.Process();
    rightArm.Process();

    limbsToJointData();
    applyYawCorrection();

    isRunning = false;
    isStopping = false;

    setPitch = 0;
    setRoll = 0;
}

void Downhill::Process()
{
    if(!isRunning)
        return; // just kick out if we're not actually running

    if(isStopping)
    {
        enterStartingPosition();
    }
    else
    {
        correctForSlope();
        applyYawCorrection();
        doCarve();
        applyTurn();
        applyBrake();
    }
}

void Downhill::Start(int numTurns)
{
    turnsRemaining = numTurns;
    TURN_INTENSITY = 0;
    BRAKE_INTENSITY = NO_BRAKE_AMOUNT;
    carveState = CARVE_STRAIGHT1;
    tickCounter = 0;

    isStopping = false;
    leftLeg.SetMoveMethod(Limb::ImmediateMove);
    rightLeg.SetMoveMethod(Limb::ImmediateMove);
    leftArm.SetMoveMethod(Limb::ApproachByOne); // keep the arms more fluid since they're not
    rightArm.SetMoveMethod(Limb::ApproachByOne);// dynamically set at every tick

    isRunning = true;
}

void Downhill::Stop()
{
    isStopping = true;
}

void Downhill::SetEnable(bool enable, bool exclusive)
{
    m_Joint.SetEnableBodyWithoutHead(enable,exclusive);
}

void Downhill::correctForSlope()
{
    const double EXPECTED_PITCH = 0;
    const double EXPECTED_ROLL = 0;

    // figure out what our inclination is, run the PIDs and figure out how we should adjust
    updateInclination();

    double delta = round(EXPECTED_PITCH - inclination.X, granularity);
    if(fabs(delta) < pitchThreshold)
        delta = 0;
    goalPitch = setPitch + delta;

    delta = round(EXPECTED_ROLL - inclination.Y, granularity);
    if(fabs(delta) < rollThreshold)
        delta = 0;
    goalRoll = setRoll + delta;

    if(goalPitch < -45)
        goalPitch = -45;
    else if(goalPitch > 45)
        goalPitch = 45;

    if(goalRoll < -45)
        goalRoll = -45;
    else if(goalRoll > 45)
        goalRoll = 45;

    pitchController->Update();
    rollController->Update();

    if(setPitch < -45)
        setPitch = -45;
    else if(setPitch > 45)
        setPitch = 45;

    // determine the difference in leg lengths and use the IK to set the legs accordingly
    double deltaLeg = tan(deg2rad(setRoll)) * Kinematics::HIP_WIDTH;
    Point3D leftLegPosition(xOffset,yOffset+TURN_INTENSITY*turnSlideGain,-hipHeight-deltaLeg/2.0);
    Point3D rightLegPosition(xOffset,yOffset-TURN_INTENSITY*turnSlideGain,-hipHeight+deltaLeg/2.0);
    leftLeg.SetGoalPosition(leftLegPosition,hipYawOffset);
    rightLeg.SetGoalPosition(rightLegPosition,hipYawOffset);

    leftLeg.Process();
    rightLeg.Process();

    // set the arm positions
    if(fabs(TURN_INTENSITY) <= 5)
    {
        leftArm.SetAngles(shoulderPitchOffset,shoulderRollOffset,elbowOffset);
        rightArm.SetAngles(shoulderPitchOffset,shoulderRollOffset,elbowOffset);
    }
    else
    {
        // TODO: either load these from the config file or dynamically calculate them based on our inclination
        double insideShoulderRoll = 25;
        double outsideShoulderRoll = 70;
        double insideShoulderPitch = 0;
        double outsideShoulderPitch = 0;
        double insideElbow = 90;
        double outsideElbow = 45;

        if(TURN_INTENSITY > 5) // RIGHT TURN
        {
            leftArm.SetAngles(outsideShoulderPitch,outsideShoulderRoll,outsideElbow);
            rightArm.SetAngles(insideShoulderPitch,insideShoulderRoll,insideElbow);
        }
        else if(TURN_INTENSITY < -5) // LEFT TURN
        {
            rightArm.SetAngles(outsideShoulderPitch,outsideShoulderRoll,outsideElbow);
            leftArm.SetAngles(insideShoulderPitch,insideShoulderRoll,insideElbow);
        }
    }
    leftArm.Process();
    rightArm.Process();

    limbsToJointData();
}

void Downhill::applyYawCorrection()
{
    leftLeg.GetPosition(m_Joint);
    rightLeg.GetPosition(m_Joint);
    Point2D leftToe = leftLeg.GetInsideToePosition().XY();
    Point2D leftHeel = leftLeg.GetInsideHeelPosition().XY();
    Point2D rightToe = rightLeg.GetInsideToePosition().XY();
    Point2D rightHeel = rightLeg.GetInsideHeelPosition().XY();

    double leftYaw = m_Joint.GetBodyAngle(JointData::ID_L_HIP_YAW);
    double rightYaw = m_Joint.GetBodyAngle(JointData::ID_R_HIP_YAW);

    double leftSkiAngle = rad2deg(atan2(leftToe.Y-leftHeel.Y, leftToe.X-leftHeel.X)) - leftYaw;
    double rightSkiAngle = rad2deg(atan2(rightToe.Y-rightHeel.Y, rightToe.X-rightHeel.X)) - rightYaw;

    double leftHipCorrection = -leftSkiAngle;
    double rightHipCorrection = -rightSkiAngle;

    if(isRollerSkis)
    {
        if(TURN_INTENSITY > 0) {
            rightHipCorrection += TURN_INTENSITY;
        }
        else if(TURN_INTENSITY < 0) {
            leftHipCorrection -= TURN_INTENSITY;
        }
    }

    //cout << leftHipCorrection << " " << rightHipCorrection << endl;

    m_Joint.SetBodyAngle(JointData::ID_L_HIP_YAW,leftYaw+leftHipCorrection);
    m_Joint.SetBodyAngle(JointData::ID_R_HIP_YAW,rightYaw+rightHipCorrection);
}

void Downhill::applyBrake()
{
    if(BRAKE_INTENSITY <= 0)
        return;

    double leftYaw = m_Joint.GetBodyAngle(JointData::ID_L_HIP_YAW) - BRAKE_INTENSITY*brakeYawGain;
    double rightYaw = m_Joint.GetBodyAngle(JointData::ID_R_HIP_YAW) - BRAKE_INTENSITY*brakeYawGain;

    double leftRoll = m_Joint.GetBodyAngle(JointData::ID_L_ANKLE_ROLL) - BRAKE_INTENSITY;
    double rightRoll = m_Joint.GetBodyAngle(JointData::ID_R_ANKLE_ROLL) - BRAKE_INTENSITY;

    m_Joint.SetBodyAngle(JointData::ID_L_HIP_YAW, leftYaw);
    m_Joint.SetBodyAngle(JointData::ID_R_HIP_YAW, rightYaw);
    m_Joint.SetBodyAngle(JointData::ID_L_ANKLE_ROLL, leftRoll);
    m_Joint.SetBodyAngle(JointData::ID_R_ANKLE_ROLL, rightRoll);
}

void Downhill::applyTurn()
{
    if(TURN_INTENSITY == 0)
    {
        return;
    }
    else
    {
        //Point3D leftFoot = leftLeg.GetPosition(m_Joint);
        //Point3D rightFoot = rightLeg.GetPosition(m_Joint);

        copyBodyAngle(*this,JointData::ID_R_ANKLE_ROLL,TURN_INTENSITY);
        copyBodyAngle(*this,JointData::ID_L_ANKLE_ROLL,-TURN_INTENSITY);

        // adjust the yaw angles so the inside ski is turned in the direction we're turning
        //if(TURN_INTENSITY > 0)
        //    copyBodyAngle(*this,JointData::ID_R_HIP_YAW,TURN_INTENSITY);
        //else
        //    copyBodyAngle(*this,JointData::ID_L_HIP_YAW,-TURN_INTENSITY);
    }
}

void Downhill::Turn(double angle)
{
    turnsRemaining = -1;
    TURN_INTENSITY = angle;
}

void Downhill::Brake(double amount)
{
    turnsRemaining = -1;
    BRAKE_INTENSITY = amount;
}

void Downhill::doCarve()
{
    if(turnsRemaining == 0)
    {
        Brake(FULL_BRAKE_AMOUNT);
        TURN_INTENSITY = 0.0;
    }
    else if(carvePeriod <= 0 || carveAmplitude <= 0 || turnsRemaining < 0)
    {
        return;
    }
    else
    {
        // update the tick counter
        tickCounter ++;
        if(tickCounter >= carvePeriod)
        {
            tickCounter = 0;
        }

/*
        if we plot the turn intensity over time we get a graph that looks something like this
        for one complete carve period:
                +---+
               /     \
              /       \
       +-----+         +-----+         +
                              \       /
                               \     /
                                +---+

       0     1  2   3  4     5  6   7  8

       There are 8 key frames to deal with:
       t0: start of new cycle, hold straight
       t1: end of initial straight hold, start of turn (dt = period * hold_straight_ratio/2.0)
       t2: end of turn, start of hold turn
       t3: end of hold turn, start turn back to straight (dt = period * hold_turn_ratio/2.0)
       t4: end of turn back, start of hold straight
       t5: end of hold straight, start of turn (dt = period * hold_straight_ratio/2.0)
       t6: end of turn, start of hold turn
       t7: end of hold turn, start of turn back (dt = period * hold_turn_ratio/2.0)
       t8: end of turn back, start of new cycle
*/
        int holdStraightTicks = (int)(carvePeriod * holdStraightRatio / 2.0);
        int holdTurnTicks = (int)(carvePeriod * holdTurnRatio / 2.0);
        int doTurnTicks = (carvePeriod - 2*holdStraightTicks - 2*holdTurnTicks)/4;

        double deltaTurnPerTick = carveAmplitude / doTurnTicks;

        switch(carveState)
        {
        case CARVE_STRAIGHT1:
        case CARVE_STRAIGHT2:
            TURN_INTENSITY = 0.0;
            if(tickCounter == holdStraightTicks)
            {
                tickCounter = 0;
                carveState = (carveState+1) % NUM_CARVE_STATES;
            }
            break;

        case CARVE_HOLD_L:
        case CARVE_HOLD_R:
            if(carveState == CARVE_HOLD_L)
                TURN_INTENSITY = -carveAmplitude;
            else
                TURN_INTENSITY = carveAmplitude;

            if(tickCounter == holdTurnTicks)
            {
                tickCounter = 0;
                carveState = (carveState+1) % NUM_CARVE_STATES;
            }
            break;

        case CARVE_TURN_R:
        case CARVE_UNTURN_L:
            TURN_INTENSITY += deltaTurnPerTick;
            if(tickCounter == doTurnTicks)
            {
                tickCounter = 0;
                carveState = (carveState+1) % NUM_CARVE_STATES;
                turnsRemaining--;
            }
            break;

        case CARVE_UNTURN_R:
        case CARVE_TURN_L:
            TURN_INTENSITY -= deltaTurnPerTick;
            if(tickCounter == doTurnTicks)
            {
                tickCounter = 0;
                carveState = (carveState+1) % NUM_CARVE_STATES;
            }
            break;

        default:
            cout << "UNKNOWN CARVE STATE: " << carveState;
            carveState = CARVE_STRAIGHT1;
            break;
        }
    }
}
