#ifndef SKIING_H
#define SKIING_H

#include <darwin/framework/Application.h>
#include <darwin/framework/Walking.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/DoublePidController.h>

#define MAX_TURN 20.0

class Skiing : public Robot::Application
{
public:
    Skiing();
    virtual ~Skiing();

    virtual bool Initialize(const char *iniFilePath, const char *motionFilePath, int safePosition);
    virtual void LoadIniSettings();
    virtual void Process();
    virtual void Execute();

    virtual void HandleVideo();

private:
    enum MODE {
        MODE_XC = 0,                // cross-country with vision
        MODE_DOWNHILL_STATIC,       // downhill without vision
        MODE_DOWNHILL_STRAIGHT,     // downhill with no carving at all -- use for balance demos
        MODE_DOWNHILL_DYNAMIC,      // downhill with vision
        MODE_DYNAMIC    ,           // fully-dynamic cross-country/downhill with vision
        /*
        MODE_DOWNHILL_PLOW,
        MODE_DOWNHILL_CARVE,
        MODE_DOWNHILL_STRAIGHT,
        MODE_DOWNHILL_LEFT,
        MODE_DOWNHILL_RIGHT,
        MODE_DOWNHILL_TELEMARK,
        MODE_ARMS,
        */
        NUM_MODES
    };
    int mode, substate;
    bool running;

    enum TELEMARK_STATE {
        TELEMARK_STRAIGHT1 = 0,
        TELEMARK_LEFT,
        TELEMARK_STRAIGHT2,
        TELEMARK_RIGHT,

        NUM_TELEMARK
    };

    enum CARVE_STATE {
        CARVE_STRAIGHT1 = 0,
        CARVE_LEFT,
        CARVE_STRAIGHT2,
        CARVE_RIGHT,

        NUM_CARVE
    };

    enum PLOW_STATE {
        PLOW_STRAIGHT = 0,
        PLOW_BRAKE
    };

    double xAmplitude, yAmplitude, aAmplitude;
    int armStartMotion;
    int safePosition;

    int downhillStraightMotion,
        downhillLeftMotion,
        downhillRightMotion,
        downhillSnowplowMotion,
        telemarkLeftMotion,
        telemarkRightMotion;

    int telemarkTurnTicks;
    int telemarkStraightTicks;
    int carvePlowTicks;
    int carveTurnTicks;
    int snowplowStraightTicks;
    int snowplowBrakeTicks;

    void sayMode(int mode);
    void processModeStart(int mode);
    void processRunningMode(int mode);
    void processModeStop(int mode);
    void processXcDynamic();
    void processDownhillDynamic();
    void processFullDynamic();

    unsigned long startTick;
    int tickCounter;

    int lastButton;

    bool enableLogging;

    Robot::MultiBlob leftMarkers, rightMarkers;
    pthread_t visionThreadId;
    volatile bool visionStale;
    std::vector<Robot::BlobTarget *>targets;
    double goalTurn, setTurn;
    double positionGain;
    Robot::DoublePidController *turnController;

    int dynamicState;
    enum {
        DYN_STATE_FLAT = 0,
        DYN_STATE_SLOPE
    };

    cv::Mat dbgFrame;
};

#endif // SKIING_H
