#!/usr/bin/python
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import argparse
import sys

def DrawZAxis( A, ax):
    zaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.00, 0.00, 0.02, 1.0] ] ).T
    zaxis_d = A.dot( zaxis )
    ax.plot( zaxis_d[0,:], zaxis_d[1,:], zaxis_d[2,:],"b-")

def DrawCoordinateAxis( A, ax ):
    xaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.05, 0.00, 0.00, 1.0] ] ).T
    yaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.00, 0.05, 0.00, 1.0] ] ).T
    zaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.00, 0.00, 0.05, 1.0] ] ).T

    xaxis_d = A.dot( xaxis )
    yaxis_d = A.dot( yaxis )
    zaxis_d = A.dot( zaxis )
    
    ax.plot( xaxis_d[0,:], xaxis_d[1,:], xaxis_d[2,:],"r-")
    ax.plot( yaxis_d[0,:], yaxis_d[1,:], yaxis_d[2,:],"g-")
    ax.plot( zaxis_d[0,:], zaxis_d[1,:], zaxis_d[2,:],"b-")

def Translation( dx, dy, dz ):
    m = np.eye(4)
    m[0,3] = dx
    m[1,3] = dy
    m[2,3] = dz
    m[3,3] = 1.0
    return m

def RotationX( theta ):
    m = np.array([ [ 1.0, 0.0, 0.0, 0.0 ],
                   [ 0.0, math.cos(theta), - math.sin( theta ), 0.0 ],
                   [ 0.0, math.sin(theta), math.cos(theta), 0.0 ],
                   [ 0.0, 0.0, 0.0, 1.0 ] ] )
    return m

def RotationY( theta ):
    m = np.array([ [ math.cos(theta), 0.0, math.sin(theta), 0.0 ],
                   [ 0.0, 1.0, 0.0, 0.0 ],
                   [ - math.sin(theta), 0.0, math.cos(theta), 0.0 ],
                   [ 0.0, 0.0, 0.0, 1.0 ] ] )
    return m

def RotationZ( theta ):
    m = np.array([ [ math.cos(theta), -math.sin( theta ), 0.0, 0.0 ],
                   [ math.sin(theta), math.cos( theta ), 0.0, 0.0 ],
                  [ 0.0, 0.0, 1.0, 0.0 ],
                   [ 0.0, 0.0, 0.0, 1.0 ] ] )
    return m
    
def deg2rad(x):
    return x/180.0*math.pi
    
def rad2deg(x):
    return x/math.pi*180.0
    
class Kinematics:
    "Dimensions for all frames and segments on the DARwIn-OP robot"
    CAMERA_DISTANCE = 33.2/1000.0 #mm
    EYE_TILT_OFFSET_ANGLE = 40.0 #degree
    LEG_SIDE_OFFSET = 37.0/1000.0 #mm
    THIGH_LENGTH = 93.0/1000.0 #mm
    CALF_LENGTH = 93.0/1000.0 #mm
    ANKLE_LENGTH = 33.5/1000.0 #mm
    LEG_LENGTH = 219.5/1000.0 #mm (THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH)
    SHOULDER_WIDTH = 2*57.5/1000.0
    SHOULDER_FRONT_OFFSET = 5/1000.0

    FOOT_LENGTH = 104.0/1000.0 #mm
    FOOT_WIDTH = 66.0/1000.0 #mm
    OUTSTEP_TO_ANKLE = (66.0-22.5)/1000.0
    TOE_TO_ANKLE = 52.0/1000.0
    SHOULDER_SIDE_OFFSET = (82.0 - 57.5)/1000.0 #mm
    SHOULDER_BOTTOM_OFFSET = 16.0/1000.0 #mm
    ELBOW_OFFSET = 15.0/1000.0 #mm
    UPPER_ARM_LENGTH = 60.0/1000.0 #mm
    LOWER_ARM_LENGTH = 129.0/1000.0 #mm
    WRIST_LENGTH = 56.0/1000.0
    THUMB_OFFSET = 40.0/1000.0
    FINGER_LENGTH = 70.0/1000.0
    DISTANCE_TO_ELBOW = 89.262/1000.0 #mm
    ARM_SERVO_OFFSET = 16.0/1000.0 #mm
    HEAD_TO_SHOULDER = 82.0/1000.0 #mm (y-axis)
    EYE_TO_SHOULDER = 85.0/1000.0 #mm (z-axis)
    TORSO_LENGTH = 122.2/1000.0 #mm (hip to shoulder)
    NECK_LENGTH = 50.5/1000.0 #mm (shoulder to neck)
    NECK_TO_FACE = 34.5/1000.0 #mm (neck to camera)
    HIP_DROP_LENGTH = 30.0/1000.0 #mm (torso to hip pitch motor)
    HIP_WIDTH = 74.0/1000.0 #mm (separation between hip motors)
    ANKLE_TO_TOE = 52.0/1000.0 #mm (ankle bracket ctr to toe)
    
    # TODO: actually measure these
    SKI_POLE_LENGTH = 165.0/1000.0
    SKI_POLE_TIP = 20.0/1000.0
    SKI_POLE_DISC_RADIUS = 15.0/1000.0
    SKI_LENGTH_FRONT = 190.0/1000.0
    SKI_LENGTH_BACK = 250.0/1000.0
    SKI_TIP_HEIGHT = 42.5/1000.0
    SKI_TIP_LENGTH = 60.0/1000.0
    SKI_WIDTH = 35.0/1000.0
    
class Arm:
    def __init__(self,flipYAxis=False):
        self.flipYAxis = flipYAxis
        shoulderWidth = -Kinematics.SHOULDER_WIDTH/2
        
        if(flipYAxis):
            shoulderWidth = -shoulderWidth
        
        # the absolute origin of the joint (DO NOT USE FOR FK CALCULATIONS!)
        self.origin = RotationX(deg2rad(90)).dot(Translation(Kinematics.SHOULDER_FRONT_OFFSET,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH,shoulderWidth))
    
    def Render(self,shoulderPitch,shoulderRoll,elbow,hand, skis=False):
        "Draw the arm in 3D.  All parameters are in radians"
        #shoulderPitchHome = RotationX(90) # pitch motor is the origin
        shoulderPitchHome = RotationX(deg2rad(90)).dot(Translation(Kinematics.SHOULDER_FRONT_OFFSET,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH,-Kinematics.SHOULDER_WIDTH/2))
        shoulderPitchJoint = shoulderPitchHome.dot(RotationZ(shoulderPitch))

        shoulderRollHome = shoulderPitchJoint.dot(Translation(0.0,-Kinematics.SHOULDER_BOTTOM_OFFSET,-Kinematics.SHOULDER_SIDE_OFFSET).dot(RotationY(deg2rad(90)).dot(RotationZ(deg2rad(90)))))
        shoulderRollJoint = shoulderRollHome.dot(RotationZ(shoulderRoll))

        elbowHome = shoulderRollJoint.dot(Translation(-Kinematics.UPPER_ARM_LENGTH,0,Kinematics.ELBOW_OFFSET).dot(RotationX(deg2rad(-90))))
        elbowJoint = elbowHome.dot(RotationZ(elbow))
        
        if(args.f):
            wristHome = elbowJoint.dot(Translation(-Kinematics.WRIST_LENGTH,Kinematics.ELBOW_OFFSET,0))
            if skis: # rotate the hand 90 degrees from normal
                wristHome = wristHome.dot(RotationX(deg2rad(90)))
            wristJoint = wristHome.dot(RotationZ(hand))
            
            thumbHome = wristHome.dot(Translation(-Kinematics.FINGER_LENGTH,0,0)) #Kinematics.THUMB_OFFSET,0))
            thumbJoint = thumbHome
            
            fingerHome = wristJoint.dot(Translation(-Kinematics.FINGER_LENGTH,0,0))
            fingerJoint = fingerHome
        else:
            fingerHome = elbowJoint.dot(Translation(-Kinematics.LOWER_ARM_LENGTH,Kinematics.ELBOW_OFFSET,0))
            if skis:
                fingerHome = fingerHome.dot(RotationX(deg2rad(90)))
            fingerJoint = fingerHome
            
        if skis: # draw the poles
            poleHome = fingerJoint.dot(Translation(0,0,0.01))
            poleDisc = poleHome.dot(Translation(0,0,-Kinematics.SKI_POLE_LENGTH))
            poleTip = poleDisc.dot(Translation(0,0,-Kinematics.SKI_POLE_TIP))
            
            discPoints = range(4)
            for i in range(len(discPoints)):
                if i == 1:
                    x = Kinematics.SKI_POLE_DISC_RADIUS
                elif i == 3:
                    x = -Kinematics.SKI_POLE_DISC_RADIUS
                else:
                    x = 0
                    
                if i == 0:
                    y = Kinematics.SKI_POLE_DISC_RADIUS
                elif i == 2:
                    y = -Kinematics.SKI_POLE_DISC_RADIUS
                else:
                    y = 0
                discPoints[i] = poleDisc.dot(Translation(x,y,0))
        
        if(self.flipYAxis):
            shoulderPitchHome[1,3] = -shoulderPitchHome[1,3]
            shoulderPitchJoint[1,3] = -shoulderPitchJoint[1,3]
            
            shoulderRollHome[1,3] = -shoulderRollHome[1,3]
            shoulderRollJoint[1,3] = -shoulderRollJoint[1,3]
            
            elbowHome[1,3] = -elbowHome[1,3]
            elbowJoint[1,3] = -elbowJoint[1,3]
            
            if(args.f):
                wristHome[1,3] = -wristHome[1,3]
                wristJoint[1,3] = -wristJoint[1,3]
                
                thumbHome[1,3] = -thumbHome[1,3]
                #thumbJoint[1,3] = -thumbJoint[1,3]
            
            fingerHome[1,3] = -fingerHome[1,3]
            #fingerJoint[1,3] = -fingerJoint[1,3]
            
            if(skis):
                poleHome[1,3] = -poleHome[1,3]
                poleDisc[1,3] = -poleDisc[1,3]
                poleTip[1,3] = -poleTip[1,3]
                for p in discPoints:
                    p[1,3] = -p[1,3]

        psn = [fingerJoint[0,3],fingerJoint[1,3],fingerJoint[2,3]]

        DrawZAxis(shoulderPitchJoint,ax)
        DrawZAxis(shoulderRollJoint,ax)
        DrawZAxis(elbowJoint,ax)
        
        if(args.f):
            DrawZAxis(wristJoint,ax)

        ax.plot( [ shoulderPitchHome[0,3], shoulderRollHome[0,3] ],
                 [ shoulderPitchHome[1,3], shoulderRollHome[1,3] ],
                 [ shoulderPitchHome[2,3], shoulderRollHome[2,3] ], 
                 "y-", linewidth=5.0 )

        ax.plot( [ shoulderRollHome[0,3], elbowHome[0,3] ],
                 [ shoulderRollHome[1,3], elbowHome[1,3] ],
                 [ shoulderRollHome[2,3], elbowHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        if(args.f):
            ax.plot( [ elbowHome[0,3], wristHome[0,3] ],
                 [ elbowHome[1,3], wristHome[1,3] ],
                 [ elbowHome[2,3], wristHome[2,3] ], 
                 "c-", linewidth=5.0 )
            
            ax.plot( [ wristHome[0,3], thumbHome[0,3] ],
                     [ wristHome[1,3], thumbHome[1,3] ],
                     [ wristHome[2,3], thumbHome[2,3] ], 
                     "k-", linewidth=2.0 )
                     
            ax.plot( [ wristHome[0,3], fingerHome[0,3] ],
                     [ wristHome[1,3], fingerHome[1,3] ],
                     [ wristHome[2,3], fingerHome[2,3] ], 
                     "y-", linewidth=5.0 )
        else:
            ax.plot( [ elbowHome[0,3], fingerHome[0,3] ],
                     [ elbowHome[1,3], fingerHome[1,3] ],
                     [ elbowHome[2,3], fingerHome[2,3] ], 
                     "c-", linewidth=5.0 )
                     
        if(skis):
            ax.plot( [ poleHome[0,3], poleTip[0,3] ],
                     [ poleHome[1,3], poleTip[1,3] ],
                     [ poleHome[2,3], poleTip[2,3] ], 
                     "k-", linewidth=2.0 )
                     
            for p in discPoints:
                ax.plot( [ poleDisc[0,3], p[0,3] ],
                         [ poleDisc[1,3], p[1,3] ],
                         [ poleDisc[2,3], p[2,3] ], 
                         "k-", linewidth=1.0 )
                 
        return psn
    
class Leg:
    def __init__(self,flipYAxis=False):
        self.flipYAxis=flipYAxis
        
        if(flipYAxis):
            self.origin = Translation(0,-Kinematics.HIP_WIDTH/2,0)
        else:
            self.origin = Translation(0,Kinematics.HIP_WIDTH/2,0)
        
    def Render(self,yaw, hipPitch, hipRoll, knee, anklePitch, ankleRoll, skis=False):
        "Draw the leg in 3D.  All parameters are in radians"
        #hipYawHome = np.eye(4)   # the z-axis goes through the yaw motor already, so nothing to change
        hipYawHome = Translation(0,Kinematics.HIP_WIDTH/2,0)
        hipYawJoint = hipYawHome.dot(RotationZ(yaw))
        
        hipRollHome = hipYawJoint.dot(Translation(0,0,-Kinematics.HIP_DROP_LENGTH).dot(RotationX(deg2rad(90)).dot(RotationY(deg2rad(90)))))
        hipRollJoint = hipRollHome.dot(RotationZ(hipRoll))
        
        hipPitchHome = hipRollJoint.dot(RotationY(deg2rad(-90)))
        hipPitchJoint = hipPitchHome.dot(RotationZ(hipPitch))
        
        kneeHome = hipPitchJoint.dot(Translation(0,-Kinematics.THIGH_LENGTH,0))
        kneeJoint = kneeHome.dot(RotationZ(-knee))
        
        anklePitchHome = kneeJoint.dot(Translation(0,-Kinematics.CALF_LENGTH,0))
        anklePitchJoint = anklePitchHome.dot(RotationZ(anklePitch))
        
        ankleRollHome = anklePitchJoint.dot(RotationY(deg2rad(90)))
        ankleRollJoint = ankleRollHome.dot(RotationZ(-ankleRoll))
        
        footHome = ankleRollJoint.dot(Translation(0,-Kinematics.ANKLE_LENGTH,0))
        footJoint = footHome
        
        # bounding box for the foot
        outsideToe = footJoint.dot(Translation(Kinematics.OUTSTEP_TO_ANKLE, 0, Kinematics.TOE_TO_ANKLE))
        insideToe = footJoint.dot(Translation(Kinematics.OUTSTEP_TO_ANKLE-Kinematics.FOOT_WIDTH, 0, Kinematics.TOE_TO_ANKLE))
        outsideHeel = footJoint.dot(Translation(Kinematics.OUTSTEP_TO_ANKLE, 0, Kinematics.TOE_TO_ANKLE-Kinematics.FOOT_LENGTH))
        insideHeel = footJoint.dot(Translation(Kinematics.OUTSTEP_TO_ANKLE-Kinematics.FOOT_WIDTH, 0, Kinematics.TOE_TO_ANKLE-Kinematics.FOOT_LENGTH))
        
        if skis:
            # bounding box of the ski
            outsideSkiToe = footJoint.dot(Translation(Kinematics.SKI_WIDTH/2.0, 0, Kinematics.SKI_LENGTH_FRONT))
            insideSkiToe = footJoint.dot(Translation(-Kinematics.SKI_WIDTH/2.0, 0, Kinematics.SKI_LENGTH_FRONT))
            outsideSkiHeel = footJoint.dot(Translation(Kinematics.SKI_WIDTH/2.0, 0, -Kinematics.SKI_LENGTH_BACK))
            insideSkiHeel = footJoint.dot(Translation(-Kinematics.SKI_WIDTH/2.0, 0, -Kinematics.SKI_LENGTH_BACK))
            
            skiTip = footJoint.dot(Translation(0, Kinematics.SKI_TIP_HEIGHT, Kinematics.SKI_LENGTH_FRONT + Kinematics.SKI_TIP_LENGTH))
        
        
        if(self.flipYAxis):
            hipYawHome[1,3] = -hipYawHome[1,3]
            hipYawJoint[1,3] = -hipYawJoint[1,3]
            
            hipRollHome[1,3] = -hipRollHome[1,3]
            hipRollJoint[1,3] = -hipRollJoint[1,3]
            
            hipPitchHome[1,3] = -hipPitchHome[1,3]
            hipPitchJoint[1,3] = -hipPitchJoint[1,3]
            
            kneeHome[1,3] = -kneeHome[1,3]
            kneeJoint[1,3] = -kneeJoint[1,3]
            
            anklePitchHome[1,3] = -anklePitchHome[1,3]
            anklePitchJoint[1,3] = -anklePitchJoint[1,3]
            
            ankleRollHome[1,3] = -ankleRollHome[1,3]
            ankleRollJoint[1,3] = -ankleRollJoint[1,3]
            
            footHome[1,3] = -footHome[1,3]
            #footJoint[1,3] = -footJoint[1,3]
            
            insideToe[1,3] = -insideToe[1,3]
            outsideToe[1,3] = -outsideToe[1,3]
            insideHeel[1,3] = -insideHeel[1,3]
            outsideHeel[1,3] = -outsideHeel[1,3]
            
            if skis:
                insideSkiToe[1,3] = -insideSkiToe[1,3]
                outsideSkiToe[1,3] = -outsideSkiToe[1,3]
                insideSkiHeel[1,3] = -insideSkiHeel[1,3]
                outsideSkiHeel[1,3] = -outsideSkiHeel[1,3]
                
                skiTip[1,3] = -skiTip[1,3]
        
        psn = [footJoint[0,3],footJoint[1,3],footJoint[2,3]]
        
        DrawZAxis(hipYawJoint,ax)
        DrawZAxis(hipRollJoint,ax)
        DrawZAxis(hipPitchJoint,ax)
        DrawZAxis(kneeJoint,ax)
        DrawZAxis(anklePitchJoint,ax)
        DrawZAxis(ankleRollJoint,ax)
        DrawZAxis(footJoint,ax)

        ax.plot( [ hipYawHome[0,3], hipRollHome[0,3] ],
                 [ hipYawHome[1,3], hipRollHome[1,3] ],
                 [ hipYawHome[2,3], hipRollHome[2,3] ], 
                 "y-", linewidth=5.0 )

        ax.plot( [ hipRollHome[0,3], hipPitchHome[0,3] ],
                 [ hipRollHome[1,3], hipPitchHome[1,3] ],
                 [ hipRollHome[2,3], hipPitchHome[2,3] ], 
                 "c-", linewidth=5.0 )
                 
        ax.plot( [ hipPitchHome[0,3], kneeHome[0,3] ],
                 [ hipPitchHome[1,3], kneeHome[1,3] ],
                 [ hipPitchHome[2,3], kneeHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        ax.plot( [ kneeHome[0,3], anklePitchHome[0,3] ],
                 [ kneeHome[1,3], anklePitchHome[1,3] ],
                 [ kneeHome[2,3], anklePitchHome[2,3] ], 
                 "c-", linewidth=5.0 )

        ax.plot( [ anklePitchHome[0,3], ankleRollHome[0,3] ],
                 [ anklePitchHome[1,3], ankleRollHome[1,3] ],
                 [ anklePitchHome[2,3], ankleRollHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        ax.plot( [ ankleRollHome[0,3], footHome[0,3] ],
                 [ ankleRollHome[1,3], footHome[1,3] ],
                 [ ankleRollHome[2,3], footHome[2,3] ], 
                 "y-", linewidth=5.0 )
                 
        # draw an X going from the foot joint to each corner of the foot
        ax.plot( [ footHome[0,3], outsideToe[0,3] ],
                 [ footHome[1,3], outsideToe[1,3] ],
                 [ footHome[2,3], outsideToe[2,3] ],
                 "k-", linewidth=1.0 )
        
        ax.plot( [ footHome[0,3], insideToe[0,3] ],
                 [ footHome[1,3], insideToe[1,3] ],
                 [ footHome[2,3], insideToe[2,3] ],
                 "k-", linewidth=1.0 )
                 
        ax.plot( [ footHome[0,3], insideHeel[0,3] ],
                 [ footHome[1,3], insideHeel[1,3] ],
                 [ footHome[2,3], insideHeel[2,3] ],
                 "k-", linewidth=1.0 )
                 
        ax.plot( [ footHome[0,3], outsideHeel[0,3] ],
                 [ footHome[1,3], outsideHeel[1,3] ],
                 [ footHome[2,3], outsideHeel[2,3] ],
                 "k-", linewidth=1.0 )
                 
        # draw the foot's bounding box
        ax.plot( [ insideToe[0,3], outsideToe[0,3] ],
                 [ insideToe[1,3], outsideToe[1,3] ],
                 [ insideToe[2,3], outsideToe[2,3] ],
                 "k-", linewidth=2.0 )
        
        ax.plot( [ outsideToe[0,3], outsideHeel[0,3] ],
                 [ outsideToe[1,3], outsideHeel[1,3] ],
                 [ outsideToe[2,3], outsideHeel[2,3] ],
                 "k-", linewidth=2.0 )
                 
        ax.plot( [ outsideHeel[0,3], insideHeel[0,3] ],
                 [ outsideHeel[1,3], insideHeel[1,3] ],
                 [ outsideHeel[2,3], insideHeel[2,3] ],
                 "k-", linewidth=2.0 )
                 
        ax.plot( [ insideHeel[0,3], insideToe[0,3] ],
                 [ insideHeel[1,3], insideToe[1,3] ],
                 [ insideHeel[2,3], insideToe[2,3] ],
                 "k-", linewidth=2.0 )
                 
        if skis:
            # draw an X going from the foot joint to each corner of the ski
            ax.plot( [ footHome[0,3], outsideSkiToe[0,3] ],
                     [ footHome[1,3], outsideSkiToe[1,3] ],
                     [ footHome[2,3], outsideSkiToe[2,3] ],
                     "k-", linewidth=1.0 )
            
            ax.plot( [ footHome[0,3], insideSkiToe[0,3] ],
                     [ footHome[1,3], insideSkiToe[1,3] ],
                     [ footHome[2,3], insideSkiToe[2,3] ],
                     "k-", linewidth=1.0 )
                     
            ax.plot( [ footHome[0,3], insideSkiHeel[0,3] ],
                     [ footHome[1,3], insideSkiHeel[1,3] ],
                     [ footHome[2,3], insideSkiHeel[2,3] ],
                     "k-", linewidth=1.0 )
                     
            ax.plot( [ footHome[0,3], outsideSkiHeel[0,3] ],
                     [ footHome[1,3], outsideSkiHeel[1,3] ],
                     [ footHome[2,3], outsideSkiHeel[2,3] ],
                     "k-", linewidth=1.0 )
                     
            # draw the ski's bounding box
            ax.plot( [ insideSkiToe[0,3], outsideSkiToe[0,3] ],
                     [ insideSkiToe[1,3], outsideSkiToe[1,3] ],
                     [ insideSkiToe[2,3], outsideSkiToe[2,3] ],
                     "k-", linewidth=2.0 )
            
            ax.plot( [ outsideSkiToe[0,3], outsideSkiHeel[0,3] ],
                     [ outsideSkiToe[1,3], outsideSkiHeel[1,3] ],
                     [ outsideSkiToe[2,3], outsideSkiHeel[2,3] ],
                     "k-", linewidth=2.0 )
                     
            ax.plot( [ outsideSkiHeel[0,3], insideSkiHeel[0,3] ],
                     [ outsideSkiHeel[1,3], insideSkiHeel[1,3] ],
                     [ outsideSkiHeel[2,3], insideSkiHeel[2,3] ],
                     "k-", linewidth=2.0 )
                     
            ax.plot( [ insideSkiHeel[0,3], insideSkiToe[0,3] ],
                     [ insideSkiHeel[1,3], insideSkiToe[1,3] ],
                     [ insideSkiHeel[2,3], insideSkiToe[2,3] ],
                     "k-", linewidth=2.0 )
                     
            ax.plot( [ outsideSkiToe[0,3], skiTip[0,3] ],
                     [ outsideSkiToe[1,3], skiTip[1,3] ],
                     [ outsideSkiToe[2,3], skiTip[2,3] ],
                     "k-", linewidth=2.0 )
                     
            ax.plot( [ insideSkiToe[0,3], skiTip[0,3] ],
                     [ insideSkiToe[1,3], skiTip[1,3] ],
                     [ insideSkiToe[2,3], skiTip[2,3] ],
                     "k-", linewidth=2.0 )
                     
            
    
        return psn
    
class Head:
    def __init__(self):
        # the absolute origin of the joint (DO NOT USE FOR FK CALCULATIONS!)
        self.origin = Translation(Kinematics.SHOULDER_FRONT_OFFSET,0,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH)
        
    def Render(self,pan,tilt):
        "Draw the head in 3D.  Parameters are in radians"
        
        panHome = Translation(Kinematics.SHOULDER_FRONT_OFFSET,0,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH)
        panJoint = panHome.dot(RotationZ(pan))
        
        tiltHome = panJoint.dot(Translation(0,0,Kinematics.NECK_LENGTH).dot(RotationX(deg2rad(90))))
        tiltJoint = tiltHome.dot(RotationZ(tilt))
        
        cameraHome = tiltJoint.dot(Translation(Kinematics.NECK_TO_FACE,0,0))
        
        psn = [cameraHome[0][3],cameraHome[1][3],cameraHome[2][3]]
        
        DrawZAxis(panHome,ax)
        DrawZAxis(tiltHome,ax)
        DrawZAxis(cameraHome,ax)
        
        ax.plot( [ panHome[0,3], tiltHome[0,3] ],
                 [ panHome[1,3], tiltHome[1,3] ],
                 [ panHome[2,3], tiltHome[2,3] ], 
                 "y-", linewidth=5.0 )
        
        ax.plot( [ tiltHome[0,3], cameraHome[0,3] ],
                 [ tiltHome[1,3], cameraHome[1,3] ],
                 [ tiltHome[2,3], cameraHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        return psn
    
def DrawReferenceAxis():
    "draw a reference box because it looks pretty?"
    BOX_SIZE = 0.3
    ax.plot([-BOX_SIZE,  BOX_SIZE],
            [-BOX_SIZE, -BOX_SIZE],
            [-BOX_SIZE, -BOX_SIZE],
            'r-')
    ax.plot([-BOX_SIZE, -BOX_SIZE],
            [-BOX_SIZE,  BOX_SIZE],
            [-BOX_SIZE, -BOX_SIZE],
            'g-')
    ax.plot([-BOX_SIZE, -BOX_SIZE],
            [-BOX_SIZE, -BOX_SIZE],
            [-BOX_SIZE,  BOX_SIZE],
            'b-')

if(__name__=='__main__'):
    parser = argparse.ArgumentParser()
    parser.add_argument("-e",action="store",type=float,default=20.0,help="Set view elevation")
    parser.add_argument("-a",action="store",type=float,default=30.0,help="Set view azimuth")
    parser.add_argument("-d",action="store_true",help="Print the image buffer to stdout instead of displaying the image")
    parser.add_argument("vars",action="store",type=float,nargs="*",help="Positions for each joint in the robot in degrees, according to their motor ID.  Motors not provided will use 0")
    parser.add_argument("-f",action="store_false",help="Do not render the fingers")
    parser.add_argument("--skis",action="store_true",help="Draw skis on the feet and poles in the hands")
    args = parser.parse_args()

    # MKS Radians System
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d',aspect='equal')
    ax.set_title("DARwIn-OP Kinematics")

    ax.view_init(elev=args.e, azim=args.a)

    origin = np.eye(4)
    DrawCoordinateAxis( origin, ax ) # origin

    leftArm = Arm(False)
    rightArm = Arm(True)    
    leftLeg = Leg(False)
    rightLeg = Leg(True)
    head = Head()

    # angles for all joints
    # these are in degrees (but are converted in the parameters below)
    leftShoulderPitch = 0
    leftShoulderRoll = 0
    leftElbow = 0
    leftHand = 0

    rightShoulderPitch = 0
    rightShoulderRoll = 0
    rightElbow = 0
    rightHand = 0

    leftHipYaw = 0
    leftHipPitch = 0
    leftHipRoll = 0
    leftKnee = 0
    leftAnklePitch = 0
    leftAnkleRoll = 0

    rightHipYaw = 0
    rightHipPitch = 0
    rightHipRoll = 0
    rightKnee = 0
    rightAnklePitch = 0
    rightAnkleRoll = 0

    pan = 0
    tilt = 0

    # parse the joint angles from the command-line
    if(args.vars!=None):
        for i in range(len(args.vars)):
            if(i==0):
                rightShoulderPitch = args.vars[i]
            elif(i==1):
                leftShoulderPitch = args.vars[i]
            elif(i==2):
                rightShoulderRoll = args.vars[i]
            elif(i==3):
                leftShoulderRoll = args.vars[i]
            elif(i==4):
                rightElbow = args.vars[i]
            elif(i==5):
                leftElbow = args.vars[i]
            elif(i==6):
                rightHipYaw = args.vars[i]
            elif(i==7):
                leftHipYaw = args.vars[i]
            elif(i==8):
                rightHipRoll = args.vars[i]
            elif(i==9):
                leftHipRoll = args.vars[i]
            elif(i==10):
                rightHipPitch = args.vars[i]
            elif(i==11):
                leftHipPitch = args.vars[i]
            elif(i==12):
                rightKnee = args.vars[i]
            elif(i==13):
                leftKnee = args.vars[i]
            elif(i==14):
                rightAnklePitch = args.vars[i]
            elif(i==15):
                leftAnklePitch = args.vars[i]
            elif(i==16):
                rightAnkleRoll = args.vars[i]
            elif(i==17):
                leftAnkleRoll = args.vars[i]
            elif(i==18):
                pan = args.vars[i]
            elif(i==19):
                tilt = args.vars[i]
            elif(i==20):
                rightHand = args.vars[i]
            elif(i==21):
                leftHand = args.vars[i]

    # render the limbs
    rightLeg.Render(deg2rad(rightHipYaw),deg2rad(rightHipPitch),deg2rad(rightHipRoll),deg2rad(rightKnee),deg2rad(rightAnklePitch),deg2rad(rightAnkleRoll),skis=args.skis)
    leftLeg.Render(deg2rad(leftHipYaw),deg2rad(leftHipPitch),deg2rad(leftHipRoll),deg2rad(leftKnee),deg2rad(leftAnklePitch),deg2rad(leftAnkleRoll),skis=args.skis)
    rightArm.Render(deg2rad(rightShoulderPitch),deg2rad(rightShoulderRoll),deg2rad(rightElbow),deg2rad(rightHand),skis=args.skis)
    leftArm.Render(deg2rad(leftShoulderPitch),deg2rad(leftShoulderRoll),deg2rad(leftElbow),deg2rad(leftHand),skis=args.skis)
    head.Render(deg2rad(pan),deg2rad(tilt))

    # draw the torso connectors
    ax.plot( [ leftArm.origin[0,3], head.origin[0,3] ],
             [ leftArm.origin[1,3], head.origin[1,3] ],
             [ leftArm.origin[2,3], head.origin[2,3] ], 
             "k-", linewidth=2.0 )
    ax.plot( [ rightArm.origin[0,3], head.origin[0,3] ],
             [ rightArm.origin[1,3], head.origin[1,3] ],
             [ rightArm.origin[2,3], head.origin[2,3] ], 
             "k-", linewidth=2.0 )
    ax.plot( [ origin[0,3], head.origin[0,3] ],
             [ origin[1,3], head.origin[1,3] ],
             [ origin[2,3], head.origin[2,3] ], 
             "k-", linewidth=2.0 )
    ax.plot( [ origin[0,3], leftLeg.origin[0,3] ],
             [ origin[1,3], leftLeg.origin[1,3] ],
             [ origin[2,3], leftLeg.origin[2,3] ], 
             "k-", linewidth=2.0 )
    ax.plot( [ origin[0,3], rightLeg.origin[0,3] ],
             [ origin[1,3], rightLeg.origin[1,3] ],
             [ origin[2,3], rightLeg.origin[2,3] ], 
             "k-", linewidth=2.0 )

    DrawReferenceAxis()

    if(args.d):
        fig.canvas.draw()
        #print fig.canvas.tostring_rgb()
        #fig.savefig("/tmp/robot.png")
        plt.savefig(sys.stdout,format='png')
        
    else:
        plt.show()
