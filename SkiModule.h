#ifndef SKIMODULE_H
#define SKIMODULE_H

#include <darwin/framework/MotionModule.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/LeftLeg.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/RightLeg.h>
#include <darwin/framework/DoublePidController.h>
#include <string>

class SkiModule : public Robot::MotionModule
{
public:
    virtual ~SkiModule();

    virtual bool IsRunning();
    virtual void Initialize();
    virtual void LoadIniSettings(minIni *ini, std::string section);

    virtual double GetPitch() { return setPitch; }
    virtual double GetRoll() { return setRoll; }

protected:
    SkiModule();

    // general pose parameters
    double hipHeight;
    double yOffset;
    double xOffset;
    double hipYawOffset;
    double hipPitchOffset;
    double anklePitchOffset;
    double ankleRollOffset;
    double shoulderPitchOffset;
    double shoulderRollOffset;
    double elbowOffset;

    // are we currently running or not?
    bool isRunning;
    bool isStopping;

    // ini config parameters set when we call LoadIniSettings
    minIni *ini;
    std::string iniSection;

    // limb instances for FK and IK calculations
    // (local instances so we don't clobber the global ones)
    Robot::LeftLeg leftLeg;
    Robot::RightLeg rightLeg;
    Robot::LeftArm leftArm;
    Robot::RightArm rightArm;

    // PID controllers for adjusting the inclination so we can balance on an incline
    Robot::DoublePidController *pitchController, *rollController;
    double goalPitch, goalRoll;
    double setPitch, setRoll;
    int granularity;
    double pitchThreshold;
    double rollThreshold;

    // X is the front-back inclination (+ fwd) (offset by hipPitch + ankle pitch)
    // Y is the left-right inclination (+ right)
    // units are in degrees
    Robot::Point2D inclination;
    void updateInclination();

    static double median(double *list, int nItems);
    static double round(double value, int granularity);

    // utility for copying joint angles from one module to another with an offset
    void copyBodyAngle(Robot::MotionModule &src, int id, double offset);

    void limbsToJointData();

private:
    static const int FILTER_SIZE = 5;
    double prevRoll[FILTER_SIZE];
    double prevPitch[FILTER_SIZE];
    int prevRollIn, prevPitchIn;
};

#endif // SKIMODULE_H
