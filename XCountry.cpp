#include "XCountry.h"
#include <iostream>
#include <cmath>
#include <darwin/framework/Point.h>
#include <darwin/framework/Math.h>
#include <darwin/framework/Kinematics.h>

using namespace std;
using namespace Robot;

XCountry *XCountry::uniqueInstance = new XCountry();

XCountry::XCountry() : SkiModule()
{
    walkEngine.X_MOVE_AMPLITUDE = X_MOVE_AMPLITUDE = 0;
    walkEngine.Y_MOVE_AMPLITUDE = Y_MOVE_AMPLITUDE = 0;
    walkEngine.A_MOVE_AMPLITUDE = A_MOVE_AMPLITUDE = 0;
    walkEngine.A_OFFSET = A_OFFSET = 0;

    mRunning = false;
}

XCountry::~XCountry()
{

}

void XCountry::Initialize()
{
    walkEngine.Initialize();
    SkiModule::Initialize();
}

void XCountry::LoadIniSettings(minIni *ini, std::string section)
{
    walkEngine.LoadINISettings(ini,section);
    SkiModule::LoadIniSettings(ini,section);

    X_MOVE_AMPLITUDE = walkEngine.X_MOVE_AMPLITUDE;
    Y_MOVE_AMPLITUDE = walkEngine.Y_MOVE_AMPLITUDE;
    A_MOVE_AMPLITUDE = walkEngine.A_MOVE_AMPLITUDE;
    A_OFFSET = walkEngine.A_OFFSET;
}

void XCountry::Process()
{
    if(!IsRunning())
    {
        walkEngine.Initialize();
        walkEngine.Process();
        leftArm.SetAngles(walkEngine.m_Joint);
        rightArm.SetAngles(walkEngine.m_Joint);
        leftLeg.SetAngles(walkEngine.m_Joint);
        rightLeg.SetAngles(walkEngine.m_Joint);

        leftArm.Process();
        rightArm.Process();
        leftLeg.Process();
        rightLeg.Process();

        limbsToJointData();
    }
    else
    {
        walkEngine.X_MOVE_AMPLITUDE = X_MOVE_AMPLITUDE;
        walkEngine.Y_MOVE_AMPLITUDE = Y_MOVE_AMPLITUDE;
        walkEngine.A_MOVE_AMPLITUDE = A_MOVE_AMPLITUDE;
        walkEngine.A_OFFSET = A_OFFSET;

        walkEngine.Process();

        correctForSlope();
        //applyYawCorrection();
    }
}

void XCountry::Start(int numSteps)
{
    cout << "Reloading Config Settings" << endl;
    LoadIniSettings(this->ini,this->iniSection);

    cout << "XCountry Started" << endl;
    walkEngine.Start(numSteps);
    mRunning = true;
}

void XCountry::Stop()
{
    walkEngine.Stop();
    mRunning = false;
}

bool XCountry::IsRunning()
{
    return mRunning && walkEngine.IsRunning();
}

void XCountry::SetEnable(bool enable, bool exclusive)
{
    walkEngine.m_Joint.SetEnableBody(true);
    m_Joint.SetEnableBodyWithoutHead(enable,exclusive);
}

void XCountry::correctForSlope()
{
    const double EXPECTED_PITCH = 0;
    const double EXPECTED_ROLL = 0;

    // figure out what our inclination is, run the PIDs and figure out how we should adjust
    updateInclination();

    double delta = round(EXPECTED_PITCH - inclination.X, granularity);
    if(fabs(delta) < pitchThreshold)
        delta = 0;
    goalPitch = setPitch + delta;

    delta = round(EXPECTED_ROLL - inclination.Y, granularity);
    if(fabs(delta) < rollThreshold)
        delta = 0;
    goalRoll = setRoll + delta;

    if(goalPitch < -45)
        goalPitch = -45;
    else if(goalPitch > 45)
        goalPitch = 45;

    if(goalRoll < -45)
        goalRoll = -45;
    else if(goalRoll > 45)
        goalRoll = 45;

    pitchController->Update();
    rollController->Update();

    if(setPitch < -45)
        setPitch = -45;
    else if(setPitch > 45)
        setPitch = 45;

    // grab the current positions from the walking engine
    leftLeg.SetAngles(walkEngine.m_Joint);
    rightLeg.SetAngles(walkEngine.m_Joint);
    leftArm.SetAngles(walkEngine.m_Joint);
    rightArm.SetAngles(walkEngine.m_Joint);
    leftLeg.Process();
    rightLeg.Process();
    leftArm.Process();
    rightArm.Process();

    // this automagically applies setPitch and setRoll to the ankles
    // no extra work needed :)
    limbsToJointData();
}

void XCountry::applyYawCorrection()
{
    leftLeg.GetPosition(m_Joint);
    rightLeg.GetPosition(m_Joint);
    Point2D leftToe = leftLeg.GetInsideToePosition().XY();
    Point2D leftHeel = leftLeg.GetInsideHeelPosition().XY();
    Point2D rightToe = rightLeg.GetInsideToePosition().XY();
    Point2D rightHeel = rightLeg.GetInsideHeelPosition().XY();

    double leftYaw = walkEngine.m_Joint.GetBodyAngle(JointData::ID_L_HIP_YAW);
    double rightYaw = walkEngine.m_Joint.GetBodyAngle(JointData::ID_R_HIP_YAW);

    double leftSkiAngle = rad2deg(atan2(leftToe.Y-leftHeel.Y, leftToe.X-leftHeel.X)) - leftYaw;
    double rightSkiAngle = rad2deg(atan2(rightToe.Y-rightHeel.Y, rightToe.X-rightHeel.X)) - rightYaw;

    double leftHipCorrection = -leftSkiAngle;
    double rightHipCorrection = -rightSkiAngle;

    //cout << leftHipCorrection << " " << rightHipCorrection << endl;

    m_Joint.SetBodyAngle(JointData::ID_L_HIP_YAW,leftYaw+leftHipCorrection);
    m_Joint.SetBodyAngle(JointData::ID_R_HIP_YAW,rightYaw+rightHipCorrection);
}
