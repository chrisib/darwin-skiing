TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

OBJECTS_DIR = build

SOURCES += main.cpp \
    Downhill.cpp \
    Skiing.cpp \
    SkiModule.cpp \
    XCountry.cpp

LIBS += -ldarwin \
        -lopencv_core \
        -lopencv_highgui \
        -lespeak \

HEADERS += \
    Downhill.h \
    Skiing.h \
    SkiModule.h \
    XCountry.h

OTHER_FILES += \
    config.ini


