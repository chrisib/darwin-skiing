#ifndef XCOUNTRY_H
#define XCOUNTRY_H

#include <SkiModule.h>
#include <darwin/framework/Walking.h>
#include <darwin/framework/Action.h>

class XCountry : public SkiModule
{
public:
    static XCountry *GetInstance(){return uniqueInstance;}
    XCountry();
    virtual ~XCountry();

    virtual void Initialize();
    virtual void Process();

    virtual void LoadIniSettings(minIni *ini, std::string section = "XCountry Config");

    virtual void Start(int numSteps = -1);
    virtual void Stop();
    virtual bool IsRunning();

    virtual void SetEnable(bool enable, bool exclusive = false);

    double X_MOVE_AMPLITUDE, Y_MOVE_AMPLITUDE, A_MOVE_AMPLITUDE, A_OFFSET;

private:    
    static XCountry *uniqueInstance;

    Robot::Walking walkEngine;

    void correctForSlope();
    void applyYawCorrection();

    bool mRunning;
};

#endif // XCOUNTRY_H
