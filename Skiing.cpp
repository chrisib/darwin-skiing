#include "Skiing.h"
#include <iostream>
#include <darwin/framework/MotionManager.h>
#include <darwin/framework/MotionStatus.h>
#include <darwin/framework/Voice.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/linux/CvCamera.h>
#include <Downhill.h>
#include <XCountry.h>

//#define POSITION_POLES

using namespace std;
using namespace Robot;

#define WINDOW_NAME "Skiing"

Skiing::Skiing()
{
    UnregisterArgument(Application::VISION_TEST_FLAG);
    //UnregisterArgument(Application::SHOW_VIDEO_FLAG);
    RegisterSwitch("--log",&enableLogging,false,"Enable MotionManager logging");

    mode = MODE_XC;
    lastButton = 0;

    targets.clear();
    targets.push_back(&leftMarkers);
    targets.push_back(&rightMarkers);

    if(showVideo)
    {
        cv::namedWindow(WINDOW_NAME);
    }
    dbgFrame = cv::Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
}

Skiing::~Skiing()
{

}

bool Skiing::Initialize(const char *iniFilePath, const char *motionFilePath, int safePosition)
{
    (void)iniFilePath;

    //if(!Application::Initialize(iniFilePath,motionFilePath,safePosition))
    //    return false;

    if(!InitIni(this->iniFilePath.c_str()))
        return false;
    if(!InitCM730(motionFilePath,safePosition))
    {
        if(enableLogging)
        {
            cout << "Failed to initialize CM730; continuing anyway so we can produce logs" << endl;
        }
        else
            return false;
    }

    CvCamera::GetInstance()->Initialize(0);

    LoadIniSettings();

    Walking::GetInstance()->m_Joint.SetEnableBody(false);
    MotionManager::GetInstance()->AddModule(Walking::GetInstance());

    LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
    MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());

    RightArm::GetInstance()->m_Joint.SetEnableBody(false);
    MotionManager::GetInstance()->AddModule(RightArm::GetInstance());

    Action::GetInstance()->Start(this->safePosition);
    Action::GetInstance()->Finish();

    cout << "Adding Downhill module" << endl;
    Downhill::GetInstance()->m_Joint.SetEnableBody(false);
    MotionManager::GetInstance()->AddModule(Downhill::GetInstance());
    cout << "Done adding Downhill module" << endl;

    cout << "Adding X-Country module" << endl;
    XCountry::GetInstance()->m_Joint.SetEnableBody(false);
    MotionManager::GetInstance()->AddModule(XCountry::GetInstance());
    cout << "Done adding X-Country module" << endl;

    LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
    RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);

#ifndef POSITION_POLES
    LeftArm::GetInstance()->CloseHand();
    RightArm::GetInstance()->CloseHand();
#else
    LeftArm::GetInstance()->OpenHand();
    RightArm::GetInstance()->OpenHand();

    MotionManager::GetInstance()->msleep(1000);
#endif

    Voice::Initialize();

    visionStale = true;
    Application::StartVisionThread();

    return true;
}

void Skiing::LoadIniSettings()
{
    CvCamera::GetInstance()->LoadINISettings(ini);
    leftMarkers.LoadIniSettings(*ini,ini->gets("Vision","left_colour","Pink"));
    rightMarkers.LoadIniSettings(*ini,ini->gets("Vision","right_colour","Blue"));

    xAmplitude = ini->getd("XCountry Options","x_amplitude",15.0);
    yAmplitude = ini->getd("XCountry Options","y_amplitude",0.0);
    aAmplitude = ini->getd("XCountry Options","a_amplitude",0.0);
    safePosition = ini->geti("Actions","ski_safe",110);
    armStartMotion = ini->geti("Actions","arm_pull_start",111);

    downhillStraightMotion = ini->geti("Actions","downhill_straight",121);
    downhillSnowplowMotion = ini->geti("Actions","downhill_snowplow",122);
    downhillLeftMotion = ini->geti("Actions","downhill_left",123);
    downhillRightMotion = ini->geti("Actions","downhill_right",124);

    telemarkLeftMotion = ini->geti("Actions","telemark_left",125);
    telemarkRightMotion = ini->geti("Actions","telemark_right",126);

    telemarkTurnTicks = ini->geti("Telemark","turn_period")/MotionModule::TIME_UNIT;
    telemarkStraightTicks = ini->geti("Telemark","straight_period")/MotionModule::TIME_UNIT;

    carveTurnTicks = ini->geti("Carve","turn_period")/MotionModule::TIME_UNIT;
    carvePlowTicks = ini->geti("Carve","straight_period")/MotionModule::TIME_UNIT;

    snowplowStraightTicks = ini->geti("Snowplow","straight_period",-1)/MotionModule::TIME_UNIT;
    snowplowBrakeTicks = ini->geti("Snowplow","brake_period")/MotionModule::TIME_UNIT;

    Walking::GetInstance()->LoadINISettings(ini);
    Downhill::GetInstance()->LoadIniSettings(ini);
    XCountry::GetInstance()->LoadIniSettings(ini);

    goalTurn = setTurn = 0.0;
    positionGain = ini->getd("Vision","positionGain",0.1);
    turnController = new DoublePidController(&setTurn, &goalTurn, ini->getd("Steering","p",0.0),ini->getd("Steering","i",0.0),ini->getd("Steering","d",0.0));
}

void Skiing::HandleVideo()
{
    if(CvCamera::GetInstance()->CaptureFrame())
    {
        if(showVideo)
            Robot::BlobTarget::FindTargets(targets,CvCamera::GetInstance()->yuvFrame,&dbgFrame,4);
        else
            Robot::BlobTarget::FindTargets(targets,CvCamera::GetInstance()->yuvFrame,NULL,4);

        // leftMarkers makes us turn left, rightMarkers makes us turn right
        // IFF the center of the marker intrudes into the relevant 50% of the frame
        // i.e. if a leftMarker is in the right half of the frame we ignore it
        // i.e. if a rightMarker is in the right half of the frame we turn right
        goalTurn = 0.0;

        vector<BoundingBox*> *boxes = leftMarkers.GetBoundingBoxes();
        for(vector<BoundingBox*>::iterator it = boxes->begin(); it!=boxes->end(); it++)
        {
            BoundingBox *bbox = *it;

            goalTurn += (bbox->center.X-Camera::WIDTH) * positionGain * bbox->width;
        }

        boxes = rightMarkers.GetBoundingBoxes();
        for(vector<BoundingBox*>::iterator it = boxes->begin(); it!=boxes->end(); it++)
        {
            BoundingBox *bbox = *it;

            goalTurn += bbox->center.X * positionGain * bbox->width;
        }

        if(goalTurn < -MAX_TURN)
            goalTurn = -MAX_TURN;
        else if(goalTurn > MAX_TURN)
            goalTurn = MAX_TURN;

        visionStale = false;

        if(showVideo)
        {
            leftMarkers.Draw(dbgFrame);
            rightMarkers.Draw(dbgFrame);

            cv::Point p(12,12);
            char buffer[64];
            sprintf(buffer,"Goal Turn: %0.2f",goalTurn);
            cv::putText(dbgFrame,buffer,p,CV_FONT_HERSHEY_PLAIN,1.0,cv::Scalar(255,255,255));

            cv::imshow(WINDOW_NAME,dbgFrame);
            cvWaitKey(1);
        }
    }
}

void Skiing::Process()
{
    int button = MotionStatus::BUTTON;

    if(button != lastButton)
    {
        lastButton = button;

        if(button == CM730::MIDDLE_BUTTON && !running)
        {
            running = true;
            Voice::Speak("Starting");

            processModeStart(mode);
        }
        else if(button == CM730::LEFT_BUTTON && running)
        {
            running = false;
            Voice::Speak("Stopping");
            processModeStop(mode);
            MotionManager::GetInstance()->msleep(1000);
        }
        else if(button == CM730::LEFT_BUTTON && !running)
        {
            mode = (mode + 1) % NUM_MODES;

            sayMode(mode);

            MotionManager::msleep(1000);
        }
    }


    if(running)
    {
        processRunningMode(mode);
    }
}

void Skiing::processModeStop(int mode)
{
    cout << "Stopping" << endl;
    switch(mode)
    {
    case MODE_XC:
        XCountry::GetInstance()->Stop();
        XCountry::GetInstance()->Finish();
        break;

    case MODE_DOWNHILL_STATIC:
    case MODE_DOWNHILL_STRAIGHT:
    case MODE_DOWNHILL_DYNAMIC:
        Downhill::GetInstance()->Stop();
        Downhill::GetInstance()->Finish();
        break;

    case MODE_DYNAMIC:
        XCountry::GetInstance()->Stop();
        XCountry::GetInstance()->Finish();
        Downhill::GetInstance()->Stop();
        Downhill::GetInstance()->Finish();
        break;

    default:
        Action::GetInstance()->Stop();
        Action::GetInstance()->Finish();
        break;
    }

    if(enableLogging)
        MotionManager::GetInstance()->StopLogging();
}

void Skiing::Execute()
{
#ifdef POSITION_POLES
    Voice::Speak("Position right pole");
    MotionManager::GetInstance()->msleep(3000);
    RightArm::GetInstance()->CloseHand();
    RightArm::GetInstance()->Finish();

    Voice::Speak("Position left pole");
    MotionManager::GetInstance()->msleep(3000);
    LeftArm::GetInstance()->CloseHand();
    LeftArm::GetInstance()->Finish();
#endif

    sayMode(mode);
    running = false;
    for(;;)
    {
        Process();
    }
}

void Skiing::sayMode(int mode)
{
    switch(mode)
    {
    case MODE_XC:
        Voice::Speak("Cross country traditional");
        cout << "MODE: Cross country traditional" << endl;
        break;

    case MODE_DOWNHILL_STATIC:
        Voice::Speak("Downhill static");
        cout << "MODE: Downhill static" << endl;
        break;

    case MODE_DOWNHILL_STRAIGHT:
        Voice::Speak("Downhill straight");
        cout << "MODE: Downhill straight" << endl;
        break;

    case MODE_DOWNHILL_DYNAMIC:
        Voice::Speak("Downhill dynamic");
        cout << "MODE: Downhill dynamic" << endl;
        break;

    case MODE_DYNAMIC:
        Voice::Speak("Fully Dynamic");
        cout << "MODE: Fully dynamic" << endl;
        break;

/*
    case MODE_DOWNHILL_STRAIGHT:
        Voice::Speak("Downhill straight");
        cout << "Downhill straight" << endl;
        break;

    case MODE_DOWNHILL_PLOW:
        Voice::Speak("Snow plow");
        cout << "Snow plow" << endl;
        break;

    case MODE_DOWNHILL_LEFT:
        Voice::Speak("Downhill left");
        cout << "Downhill left" << endl;
        break;

    case MODE_DOWNHILL_RIGHT:
        Voice::Speak("Downhill right");
        cout << "Downhill right" << endl;
        break;

    case MODE_DOWNHILL_CARVE:
        Voice::Speak("Downhill carve");
        cout << "Downhill carve" << endl;
        break;

    case MODE_DOWNHILL_TELEMARK:
        Voice::Speak("Telemark");
        cout << "Telemark" << endl;
        break;

    case MODE_ARMS:
        Voice::Speak("Cross country arms only");
        cout << "Cross country arms only" << endl;
        break;
*/
    }
}

void Skiing::processModeStart(int mode)
{
    int numTurns = ini->geti("Downhill Options","num_turns",Downhill::NO_CARVE);
    startTick = MotionStatus::ticks;
    tickCounter = 0;

    switch(mode)
    {
    case MODE_XC:
    case MODE_DYNAMIC:
        //Walking::GetInstance()->m_Joint.SetEnableBody(true,true);
        XCountry::GetInstance()->m_Joint.SetEnableBody(true,true);
        break;

    case MODE_DOWNHILL_STATIC:
    case MODE_DOWNHILL_DYNAMIC:
    case MODE_DOWNHILL_STRAIGHT:
        Downhill::GetInstance()->m_Joint.SetEnableBody(true,true);
        break;

    default:
        Action::GetInstance()->m_Joint.SetEnableBody(true,true);
        break;
    }
    LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
    RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);

    switch(mode)
    {
    case MODE_XC:
    case MODE_DYNAMIC:
        //Walking::GetInstance()->Start();
        //Walking::GetInstance()->X_MOVE_AMPLITUDE = xAmplitude;
        //Walking::GetInstance()->Y_MOVE_AMPLITUDE = yAmplitude;
        //Walking::GetInstance()->A_MOVE_AMPLITUDE = aAmplitude;
        XCountry::GetInstance()->Start();
        XCountry::GetInstance()->X_MOVE_AMPLITUDE = xAmplitude;
        XCountry::GetInstance()->Y_MOVE_AMPLITUDE = yAmplitude;
        XCountry::GetInstance()->A_MOVE_AMPLITUDE = aAmplitude;

        dynamicState = DYN_STATE_FLAT;
        break;

    case MODE_DOWNHILL_STATIC:
        Downhill::GetInstance()->Start(numTurns);
        break;

    case MODE_DOWNHILL_DYNAMIC:
    case MODE_DOWNHILL_STRAIGHT:
        Downhill::GetInstance()->Start();
        break;

/*
    case MODE_ARMS:
        Action::GetInstance()->Start(armStartMotion);
        break;

    case MODE_DOWNHILL_CARVE:
        substate = CARVE_STRAIGHT1;
        Action::GetInstance()->Start(downhillSnowplowMotion);
        break;

    case MODE_DOWNHILL_STRAIGHT:
        Action::GetInstance()->Start(downhillStraightMotion);
        break;

    case MODE_DOWNHILL_TELEMARK:
        substate = TELEMARK_STRAIGHT1;
        Action::GetInstance()->Start(downhillSnowplowMotion);
        break;

    case MODE_DOWNHILL_PLOW:
        if(snowplowStraightTicks > 0)
        {
            substate = PLOW_STRAIGHT;
            Action::GetInstance()->Start(downhillStraightMotion);
        }
        else
        {
            substate = PLOW_BRAKE;
            Action::GetInstance()->Start(downhillSnowplowMotion);
        }
        break;

    case MODE_DOWNHILL_LEFT:
        Action::GetInstance()->Start(downhillLeftMotion);
        break;

    case MODE_DOWNHILL_RIGHT:
        Action::GetInstance()->Start(downhillRightMotion);
        break;
*/
    }

    if(enableLogging)
        MotionManager::GetInstance()->StartLogging();
}

void Skiing::processRunningMode(int mode)
{
    MotionManager::Sync();
    tickCounter++;

    switch(mode)
    {
/*
    case MODE_DOWNHILL_CARVE:
        switch(substate)
        {
        case CARVE_STRAIGHT1:
            if(tickCounter == carvePlowTicks)
            {
                tickCounter = 0;
                substate = CARVE_RIGHT;
                Action::GetInstance()->Start(downhillRightMotion);
                Action::GetInstance()->Finish();
            }
            break;

        case CARVE_STRAIGHT2:
            if(tickCounter == carvePlowTicks)
            {
                tickCounter = 0;
                substate = CARVE_LEFT;
                Action::GetInstance()->Start(downhillLeftMotion);
                Action::GetInstance()->Finish();
            }
            break;

        case CARVE_LEFT:
            if(tickCounter == carveTurnTicks)
            {
                tickCounter = 0;
                substate = CARVE_STRAIGHT1;
                Action::GetInstance()->Start(downhillSnowplowMotion);
                Action::GetInstance()->Finish();
            }
            break;

        case CARVE_RIGHT:
            if(tickCounter == carveTurnTicks)
            {
                tickCounter = 0;
                substate = CARVE_STRAIGHT2;
                Action::GetInstance()->Start(downhillSnowplowMotion);
                Action::GetInstance()->Finish();
            }
            break;
        }
        break;

    case MODE_DOWNHILL_TELEMARK:
        switch(substate)DYNAMIC
        {
        case TELEMARK_STRAIGHT1:
            if(tickCounter == telemarkStraightTicks)
            {
                tickCounter = 0;
                substate = TELEMARK_RIGHT;
                Action::GetInstance()->Start(telemarkRightMotion);
                Action::GetInstance()->Finish();
            }
            break;

        case TELEMARK_STRAIGHT2:
            if(tickCounter == telemarkStraightTicks)
            {
                tickCounter = 0;
                substate = TELEMARK_LEFT;
                Action::GetInstance()->Start(telemarkLeftMotion);
                Action::GetInstance()->Finish();
            }
            break;

        case TELEMARK_RIGHT:
            if(tickCounter == telemarkTurnTicks)
            {
                tickCounter = 0;
                substate = TELEMARK_STRAIGHT2;
                Action::GetInstance()->Start(downhillSnowplowMotion);
                Action::GetInstance()->Finish();
            }
            break;

        case TELEMARK_LEFT:
            if(tickCounter == telemarkTurnTicks)
            {
                tickCounter = 0;
                substate = TELEMARK_STRAIGHT1;
                Action::GetInstance()->Start(downhillSnowplowMotion);
                Action::GetInstance()->Finish();
            }
            break;

        }
        break;

    case MODE_DOWNHILL_PLOW:
        if(snowplowStraightTicks <= 0)
        {
            // do nothing
        }
        else
        {
            switch(substate)
            {
            case PLOW_BRAKE:
                if(tickCounter == snowplowBrakeTicks)
                {
                    substate = PLOW_STRAIGHT;
                    tickCounter = 0;
                    Action::GetInstance()->Start(downhillStraightMotion);
                    Action::GetInstance()->Finish();
                }
                break;
            case PLOW_STRAIGHT:
                if(tickCounter == snowplowStraightTicks)
                {
                    substate = PLOW_BRAKE;
                    tickCounter = 0;
                    Action::GetInstance()->Start(downhillSnowplowMotion);
                    Action::GetInstance()->Finish();
                }
                break;
            }
        }
        break;

    case MODE_DOWNHILL_STRAIGHT:
    case MODE_DOWNHILL_LEFT:
    case MODE_DOWNHILL_RIGHT:
    case MODE_ARMS:
        break;
*/
    case MODE_DOWNHILL_DYNAMIC:
        processDownhillDynamic();
        break;

    case MODE_DYNAMIC:
        processFullDynamic();
        break;

    case MODE_XC:
        processXcDynamic();
        break;

    case MODE_DOWNHILL_STATIC:
    case MODE_DOWNHILL_STRAIGHT:
    default:
        break;
    }
}

void Skiing::processXcDynamic()
{
    if(!visionStale)
    {
        visionStale = true;

        turnController->Update();
        if(setTurn < -MAX_TURN)
            setTurn = -MAX_TURN;
        else if(setTurn > MAX_TURN)
            setTurn = MAX_TURN;

        XCountry::GetInstance()->A_MOVE_AMPLITUDE = setTurn;
    }
}

void Skiing::processDownhillDynamic()
{
    if(!visionStale)
    {
        visionStale = true;

        turnController->Update();
        if(setTurn < -MAX_TURN)
            setTurn = -MAX_TURN;
        else if(setTurn > MAX_TURN)
            setTurn = MAX_TURN;

        Downhill::GetInstance()->Turn(setTurn);
    }
}

void Skiing::processFullDynamic()
{
    switch(dynamicState)
    {
    case DYN_STATE_FLAT:
        processXcDynamic();
        if(fabs(XCountry::GetInstance()->GetPitch()) > 30)
        {
            dynamicState = DYN_STATE_SLOPE;
            XCountry::GetInstance()->Stop();
            XCountry::GetInstance()->Finish();

            Downhill::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Downhill::GetInstance()->Start();
        }
        else if(fabs(Downhill::GetInstance()->GetPitch()) < 20)
        {
            dynamicState = DYN_STATE_SLOPE;
            Downhill::GetInstance()->Stop();
            Downhill::GetInstance()->Finish();

            XCountry::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            XCountry::GetInstance()->Start();
            XCountry::GetInstance()->X_MOVE_AMPLITUDE = xAmplitude;
            XCountry::GetInstance()->Y_MOVE_AMPLITUDE = yAmplitude;
            XCountry::GetInstance()->A_MOVE_AMPLITUDE = aAmplitude;
        }
    }
}
