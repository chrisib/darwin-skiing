#include "SkiModule.h"
#include <darwin/framework/Point.h>
#include <darwin/framework/Kinematics.h>
#include <darwin/framework/Math.h>
#include <iostream>
#include <cmath>
#include <algorithm>

using namespace Robot;
using namespace std;

SkiModule::SkiModule() : MotionModule()
{
    pitchController = NULL;
    rollController = NULL;
    isRunning = false;

    prevPitchIn = 0;
    prevRollIn = 0;
    for(int i=0; i<FILTER_SIZE; i++)
    {
        prevPitch[i] = 0;
        prevRoll[i] = 0;
    }

    // determine the robot's pitch and roll when on a flat surface
    // the PID controllers attempt to maintain these settings when going downhill or cross-hill
    inclination.X = setPitch = goalPitch = 0;
    inclination.Y = setRoll = goalRoll = 0;
}

SkiModule::~SkiModule()
{
    if(pitchController!=NULL)
        delete pitchController;
    if(rollController!=NULL)
        delete rollController;
}

void SkiModule::Initialize()
{
    leftArm.Initialize();
    rightArm.Initialize();
    leftLeg.Initialize();
    rightLeg.Initialize();
}

bool SkiModule::IsRunning()
{
    return isRunning;
}

void SkiModule::LoadIniSettings(minIni *ini, string section)
{
    this->ini = ini;
    this->iniSection = section;

    hipHeight = ini->getd(section,"hip_height");
    yOffset = ini->getd(section,"y_offset");
    xOffset = ini->getd(section,"x_offset");
    hipYawOffset = ini->getd(section,"hip_yaw_offset");
    hipPitchOffset = ini->getd(section,"hip_pitch_offset");
    anklePitchOffset = ini->getd(section,"ankle_pitch_offset");
    ankleRollOffset = ini->getd(section,"ankle_roll_offset");
    shoulderRollOffset = ini->getd(section,"shoulder_roll_offset");
    shoulderPitchOffset = ini->getd(section,"shoulder_pitch_offset");
    elbowOffset = ini->getd(section,"elbow_offset");

    if(pitchController!=NULL)
        delete pitchController;
    if(rollController!=NULL)
        delete rollController;

    pitchController = new DoublePidController(&setPitch,&goalPitch,ini->getd(section,"pitch_p"),ini->getd(section,"pitch_i"),ini->getd(section,"pitch_d"));
    rollController = new DoublePidController(&setRoll,&goalRoll,ini->getd(section,"roll_p"),ini->getd(section,"roll_i"),ini->getd(section,"roll_d"));

    granularity = ini->geti(section,"granularity");
    pitchThreshold = ini->getd(section,"pitch_threshold");
    rollThreshold = ini->getd(section,"roll_threshold");
}

void SkiModule::updateInclination()
{
    const double G = 9.81; // m/s^2

    // get the acceleration in all 3 axes in m/s^2
    double rlAccel = ((MotionStatus::RL_ACCEL/128.0) - 4.0) * G;
    double fbAccel = ((MotionStatus::FB_ACCEL/128.0) - 4.0) * G;
    double zAccel = ((MotionStatus::Z_ACCEL/128.0) - 4.0) * G;

    if(rlAccel > G || fbAccel > G || zAccel > G ||
       rlAccel <-G || fbAccel <-G || zAccel <-G)
    {
        return;
    }

    double pitch = -rad2deg(atan2(fbAccel, zAccel));
    double roll = -rad2deg(atan2(rlAccel, zAccel));

    prevPitch[prevPitchIn] = pitch - hipPitchOffset - anklePitchOffset;
    prevRoll[prevRollIn] = roll;

    prevPitchIn = (prevPitchIn + 1) % FILTER_SIZE;
    prevRollIn = (prevRollIn + 1) % FILTER_SIZE;

    inclination.X = median(prevPitch, FILTER_SIZE);
    inclination.Y = median(prevRoll, FILTER_SIZE);

    //cout << "X: " << inclination.X << " Y: " << inclination.Y << endl;
}

void SkiModule::copyBodyAngle(MotionModule &src, int id, double offset)
{
    m_Joint.SetBodyAngle(id,src.m_Joint.GetBodyAngle(id)+offset);
}

static int sortCompare(const void* a, const void *b)
{
    double x = *(double*)a;
    double y = *(double*)b;

    if(x < y)
        return -1;
    else if(x > y)
        return 1;
    else
        return 0;

}
double SkiModule::median(double *list, int nItems)
{
    double sorted[nItems];
    for(int i=0; i<nItems; i++)
        sorted[i] = list[i];

    qsort(sorted, nItems, sizeof(double), sortCompare);
    double result = sorted[nItems/2];

    return result;
}

double SkiModule::round(double value, int granularity)
{
    if(granularity <= 0 )
        return value;
    else
    {
        int v = (int)value;
        int r = v%granularity;

        if(r < granularity/2)
        {
            v = v-r;
        }
        else
        {
            v = v-r+granularity;
        }

        return (double)v;
    }
}

void SkiModule::limbsToJointData()
{
    // copy the local limb positions into m_Joint so the motion manager can use them
    copyBodyAngle(leftLeg,JointData::ID_L_ANKLE_PITCH,setPitch+anklePitchOffset);
    copyBodyAngle(leftLeg,JointData::ID_L_ANKLE_ROLL,setRoll);
    copyBodyAngle(leftLeg,JointData::ID_L_KNEE,0);
    copyBodyAngle(leftLeg,JointData::ID_L_HIP_ROLL,0);
    copyBodyAngle(leftLeg,JointData::ID_L_HIP_PITCH,hipPitchOffset);
    copyBodyAngle(leftLeg,JointData::ID_L_HIP_YAW,0);
    copyBodyAngle(rightLeg,JointData::ID_R_ANKLE_PITCH,setPitch+anklePitchOffset);
    copyBodyAngle(rightLeg,JointData::ID_R_ANKLE_ROLL,-setRoll);
    copyBodyAngle(rightLeg,JointData::ID_R_KNEE,0);
    copyBodyAngle(rightLeg,JointData::ID_R_HIP_ROLL,0);
    copyBodyAngle(rightLeg,JointData::ID_R_HIP_PITCH,hipPitchOffset);
    copyBodyAngle(rightLeg,JointData::ID_R_HIP_YAW,0);
    copyBodyAngle(leftArm,JointData::ID_L_SHOULDER_PITCH,0);
    copyBodyAngle(leftArm,JointData::ID_L_SHOULDER_ROLL,0);
    copyBodyAngle(leftArm,JointData::ID_L_ELBOW,0);
    copyBodyAngle(rightArm,JointData::ID_R_SHOULDER_PITCH,0);
    copyBodyAngle(rightArm,JointData::ID_R_SHOULDER_ROLL,0);
    copyBodyAngle(rightArm,JointData::ID_R_ELBOW,0);
}
