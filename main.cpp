#include <iostream>
#include <Skiing.h>

using namespace std;

int main(int argc, char **argv)
{
    Skiing *skiing = new Skiing();
    skiing->ParseArguments(argc,argv);

    if(!skiing->Initialize("config.ini","motion.bin",Robot::Action::DEFAULT_MOTION_SIT_DOWN))
        return -1;

    
    skiing->Execute();

    return 0;
}

